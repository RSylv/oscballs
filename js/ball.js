////////////////////////
////  BALLE DE SON  ////
////////////////////////
class Ball {
    constructor(x, y) {
        this.x = x
        this.y = y
        this.vx = Math.random() * 4
        this.vy = Math.random() * 2
        this.radius = Math.random() * 20 + 1
        this.color = 'hsl(200, 80%, 50%)'
        this.synth = synth;
        this.note = notes[Math.floor(Math.random() * notes.length)] + (Math.floor(Math.random() * 4) + 2)
        this.isConnect = false
        this.isPlaying = false
    }

    draw() {
        // let fx = 214 - (squareDistance / 10000) + 1;
        let fx = 207;
        ctx.beginPath();
        // ctx.fillStyle = this.isConnect ? 'hsl(' + fx + ', 78%, 39%)' : this.color;
        ctx.fillStyle = this.isConnect ? 'hsl(30, 80%, 50%)' : this.color;
        ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, true);
        ctx.shadowBlur = 20;
        ctx.shadowColor = '#f0f0f056';
        ctx.closePath();
        ctx.fill();
    }

    update() {
        if (gravity) {
            this.vy *= .99;
            this.vy += .25;
        }

        this.x += this.vx;
        this.y += this.vy;

        if (this.y + this.vy > canvas.height || this.y + this.vy < 0) {
            this.vy = -this.vy;
        }
        if (this.x + this.vx > canvas.width || this.x + this.vx < 0) {
            this.vx = -this.vx;
        }

        for (let i = 0; i < ballsArray.length; i++) {
            if (ballsArray[i].isPlaying && ballsArray[i].isConnect === false) ballsArray[i].isPlaying = false;
            ballsArray[i].isConnect = false
        };

        // console.log("GAIN :", MASTER.get());

        for (let a = 0; a < ballsArray.length; a++) {
            for (let b = a + 1; b < ballsArray.length; b++) {

                if (detectConnections(ballsArray[a], ballsArray[b])) {
                    ballsArray[a].isConnect = true;
                    ballsArray[b].isConnect = true;

                    if (isRunning && ballsArray[a].isConnect && ballsArray[a].isPlaying === false) {
                        ballsArray[a].synth.triggerAttackRelease(ballsArray[a].note, now + 0.2);
                        ballsArray[a].isPlaying = true
                    }
                    if (isRunning && ballsArray[b].isConnect && ballsArray[b].isPlaying === false) {
                        ballsArray[b].synth.triggerAttackRelease(ballsArray[b].note, now + 0.2);
                        ballsArray[b].isPlaying = true
                    }
                }
            }
        }
    }

    setSynth(synth) {
        this.synth = synth;
    }

    getSynth() {
        return this.synth;
    }
}