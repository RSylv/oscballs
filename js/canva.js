const canvas = document.getElementById('_canvas')
const ctx = canvas.getContext('2d')
canvas.width = window.innerWidth
canvas.height = window.innerHeight

////////////////////////
//// DOM Controls   ////
////////////////////////
const startBtn = document.getElementById('_start')
const stopBtn = document.getElementById('_stop')
const enter = document.getElementById('_enter')
// const balls = document.getElementById('_balls')
// const radius = document.getElementById('_radius')
// const balls  = slider.sliders[0]
// const radius = slider.sliders[1]
const divSliders = document.querySelectorAll('.sliderValue')

const synthChoice = document.getElementById('synthChoice')

////////////////////////
//// NOTES - TONEJS ////
////////////////////////
const notes = ["C", "C#", "Db", "D", "D#", "Eb", "E", "F", "F#", "Gb", "G", "A", "A#", "Bb", "B"]
const now = Tone.now()
const MASTER = new Tone.Volume(-30).toDestination();
const reverb = new Tone.Reverb({
    decay: 15,
    predelay: .3,
    wet: 0
}).connect(MASTER)
const synths = [
    new Tone.MonoSynth({
        oscillator: {
            type: "sine"
        },
        envelope: {
            attack: 0.1,
            decay: 0.5,
            sustain: 0.8,
            release: 10,
        }
    }),
    new Tone.MonoSynth({
        oscillator: {
            type: "square"
        },
        envelope: {
            attack: 0.1,
            decay: 0.7,
            sustain: 0.5,
            release: 0.1,
        },
        volume: 1
    }),
    new Tone.MetalSynth({
        envelope: {
            attack: 0.1,
            decay: 0.2,
            sustain: 0.5,
            release: 5,
        },
        portamento: 1,
        volume: 1
    }),
    new Tone.MetalSynth({
        portamento: 0,
        volume: -15,
        envelope: {
            attack: 0.001,
            decay: 1.4,
            release: 1,
        },
        harmonicity: 18.1,
        modulationIndex: 12,
        resonance: 1000,
        octaves: 1.5,
    }),
    new Tone.DuoSynth({
        detune: -1200,
        portamento: .5,
        vibratoRate: 320
    }),
    new Tone.MembraneSynth(),
    new Tone.PluckSynth({
        attackNoise: 250,
        dampening: 500,
        release: 200,
        resonance: 0.1
    }),
    this.synth = new Tone.PluckSynth({
        attackNoise: 100,
        dampening: 100,
        release: 10,
        resonance: 1
    })
];

/////////////////
//// GLOBALS ////
/////////////////
let ballsArray = []
let trail = false
let gravity = false
let force = 0.2
let balls_number = 6
let radius_value = 100
let squareDistance = 0
let isRunning = false
let time = 0;
let hasConnectedLines = true
// const CONNECT_ADJUSTEMENT = 50
// let synthIndex = 0; 
let synth;
synthChoice.oninput = () => {
    let synthIndex = Number(synthChoice.value)
    synth = synths[synthIndex]
}

// __________________________________________________________________________________________________________________________
////////////////////////
////  BALLE DE SON  ////
////////////////////////
class Ball {
    constructor(x, y) {
        this.x = x
        this.y = y
        this.vx = Math.random() * 4
        this.vy = Math.random() * 2
        this.radius = Math.random() * 20 + 1
        this.color = 'hsl(200, 80%, 50%)'
        this.synth = new Tone.MonoSynth({
            oscillator: {
                type: "sine"
            },
            envelope: {
                attack: 0.1,
                decay: 0.5,
                sustain: 0.8,
                release: 10,
            }
        }).connect(reverb);
        this.note = notes[Math.floor(Math.random() * notes.length)] + (Math.floor(Math.random() * 4) + 2)
        this.isConnect = false
        this.isPlaying = false
    }

    draw() {
        // let fx = 214 - (squareDistance / 10000) + 1;
        let fx = 30;
        ctx.beginPath();
        // ctx.fillStyle = this.isConnect ? 'hsl(' + fx + ', 78%, 39%)' : this.color;
        ctx.fillStyle = this.isConnect ? 'hsl(' + fx + ', 80%, 50%)' : this.color;
        ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, true);
        ctx.shadowBlur = 20;
        ctx.shadowColor = '#f0f0f056';
        ctx.closePath();
        ctx.fill();
    }

    update() {
        if (gravity) {
            this.vy *= .99;
            this.vy += .25;
        }

        this.x += this.vx;
        this.y += this.vy;

        if (this.y + this.vy > canvas.height || this.y + this.vy < 0) {
            this.vy = -this.vy;
        }
        if (this.x + this.vx > canvas.width || this.x + this.vx < 0) {
            this.vx = -this.vx;
        }

        for (let i = 0; i < ballsArray.length; i++) {
            if (ballsArray[i].isPlaying && ballsArray[i].isConnect === false) ballsArray[i].isPlaying = false;
            ballsArray[i].isConnect = false
        };

        // console.log("GAIN :", MASTER.get());

        for (let a = 0; a < ballsArray.length; a++) {
            for (let b = a + 1; b < ballsArray.length; b++) {

                if (detectConnections(ballsArray[a], ballsArray[b])) {
                    ballsArray[a].isConnect = true;
                    ballsArray[b].isConnect = true;

                    if (isRunning && ballsArray[a].isConnect && ballsArray[a].isPlaying === false) {
                        ballsArray[a].synth.triggerAttackRelease(ballsArray[a].note, now + 0.2);
                        ballsArray[a].isPlaying = true
                    }
                    if (isRunning && ballsArray[b].isConnect && ballsArray[b].isPlaying === false) {
                        ballsArray[b].synth.triggerAttackRelease(ballsArray[b].note, now + 0.2);
                        ballsArray[b].isPlaying = true
                    }
                }
            }
        }
    }

    setSynth(synth) {
        this.synth = synth;
    }

    getSynth() {
        return this.synth;
    }
}




////////////////////////
////  INIT Function ////
////////////////////////
function init(balls_number) {
    ballsArray = [];
    for (let i = 0; i < balls_number; i++) {
        let x = Math.floor(Math.random() * canvas.width)
        let y = Math.floor(Math.random() * canvas.height)
        ballsArray.push(new Ball(x, y))
    }
}


////////////////////////
//// ANIMATION LOOP ////
//////////////////////// 
function animate() {

    if (trail) {
        ctx.fillStyle = 'rgba(255,255,255,0.3)';
        ctx.fillRect(0, 0, canvas.width, canvas.height);
    }
    else ctx.clearRect(0, 0, canvas.width, canvas.height);

    if (isRunning === false) {
        hasConnectedLines = false
        gravity = true
        time++
        if (time > 150) {
            cancelAnimationFrame(animate);
            ctx.clearRect(0, 0, canvas.width, canvas.height)
            return endPlaying();
        }
    }

    for (let i = 0; i < ballsArray.length; i++) {
        ballsArray[i].update();
        ballsArray[i].draw();
    }

    requestAnimationFrame(animate);
}


////////////////////////
//// COLLISION SOUND ///
////////////////////////
function detectConnections(obj1, obj2) {

    // Calculate the distance between the two circles
    squareDistance = (obj1.x - obj2.x) * (obj1.x - obj2.x) + (obj1.y - obj2.y) * (obj1.y - obj2.y);
    if (squareDistance <= ((obj1.radius + obj2.radius + radius_value) * (obj1.radius + obj2.radius + radius_value))) {

        if (hasConnectedLines) {
            // ctx.clearRect(0, 0, canvas.width, canvas.height);
            let opacityValue = 1 - (squareDistance / 100000) + 1.5;
            ctx.strokeStyle = 'rgba(255,255,255,' + opacityValue + ')';
            ctx.lineWidth = 1;
            ctx.beginPath();
            ctx.moveTo(obj1.x, obj1.y)
            ctx.lineTo(obj2.x, obj2.y)
            ctx.stroke();
        }
        return true

    } else return false;
}


//////////////////
// GAIN CONTROL //
//////////////////
const mastVol = document.getElementById('master')
const mastText = document.getElementById('mastText')
const revWet = document.getElementById('reverbWet')
const revText = document.getElementById('reverbText')

let gain = Math.floor(mastVol.value / 2)    // Input => [-120, -0]: Output => [-60,0]
let rev = revWet.value / 100               // Input => [0, 100]: Output => [0,1]

mastText.textContent = gain + 'db'
revText.textContent = '-' + (100 - revWet.value) + 'db'

revWet.oninput = () => {
    rev = revWet.value / 100
    // Inverser les valeurs, Ex. 100 = 0, 0 = 100
    // choixMax (100) - x = y
    revText.textContent = '-' + (100 - revWet.value) + 'db'
    reverb.wet.value = rev
}

mastVol.oninput = () => {
    gain = Math.floor(mastVol.value / 2)
    mastText.textContent = gain + 'db'
    MASTER.volume.value = mastVol.value / 2
    if (-59 > gain) {
        MASTER.mute = true;
    }
}


////////////////////////
////  START & STOP  ////
////////////////////////
startBtn.addEventListener('click', startLecture)
function startLecture() {

    // console.log(slider.sliders[0]);
    // console.log(slider.sliders[1]);

    const choiceNum = Number(divSliders[0].innerHTML)
    const choiceRad = Number(divSliders[1].innerHTML)

    if (Number.isInteger(choiceNum) && 0 < choiceNum && 40 >= choiceNum) balls_number = choiceNum;
    10 < balls_number ? hasConnectedLines = false : hasConnectedLines = true;

    if (Number.isInteger(choiceRad) && 10 <= choiceRad && 300 >= choiceRad) radius_value = choiceRad;

    enter.classList.add('hidden')
    stopBtn.classList.remove('hidden')
    document.querySelector('.gain-container').classList.remove('hidden')

    canvas.width = window.innerWidth
    canvas.height = window.innerHeight

    gravity = false
    isRunning = true
    time = 0

    init(balls_number);
    animate();
}
stopBtn.addEventListener('click', () => isRunning = false)
endPlaying = () => {
    stopBtn.classList.add('hidden')
    enter.classList.remove('hidden')
    document.querySelector('.gain-container').classList.add('hidden')
}

